using System;
using System.Collections.Generic;
using Xunit;

namespace StudyTest
{
    public class UrlIfyTest
    {
        private UrlIfy testObj;

        public UrlIfyTest()
        {
            testObj = new UrlIfy();
        }

        
        [Theory]
        [InlineData(null, null, 0)]
        [InlineData("", "", 0)]
        public void Transmute_GivenNullOrEmpty_ExpectsSameResult(string given, string expected, int trueLength){
            
            var actual = testObj.Transmute(given, trueLength);
            
            Assert.Equal(expected, actual);            
        }

        [Fact]
        public void Transmute_GivenStringWithZeroTrueLength_ReturnsGiven()
        {
            var expected = " f i s h t a c o e s";
            var given = " f i s h t a c o e s";
            var actual = testObj.Transmute(given, 0);

            Assert.Equal(expected, actual);
        }

        [Fact]

        public void Transmute_GivenStringAndTrueLength_ExpectsProperUrlify(){
            var expected = "Mr%20John%20Smith";
            var given = "Mr John Smith    ";
            var trueLength = 13;

            var actual = testObj.Transmute(given, trueLength);

            Assert.Equal(expected, actual);
        }

        public void Transmute_GivenStringNoSpaces_ExpectsString(){
            var expected = "fish";
            var given = "fish";
            var trueLength = given.Length;

            var actual = testObj.Transmute(given, trueLength);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("f i s h t a c o e s                  ", "f%20i%20s%20h%20t%20a%20c%20o%20e%20s", 10)]        
        public void Transmute_GivenStuff_ExpectsResult(string given, string expected, int trueLength){
            
            var actual = testObj.Transmute(given, trueLength);
            
            Assert.Equal(expected, actual);            
        }

        

    }

    internal class UrlIfy
    {
        internal string Transmute(string given, int trueLength)
        {
            if(string.IsNullOrEmpty(given)){
                return given;
            }

            if(trueLength <= 0 || trueLength == given.Length){
                return given;
            }

            var i = 0;
            var givenAsCharArray = given.ToCharArray();
            for(i=givenAsCharArray.Length-1; i>=trueLength; i--)
            {
                if(givenAsCharArray[i]!= ' '){
                    break;
                }
            }

            for(int j=givenAsCharArray.Length-1; j>= 0; j--)
            {
                if(givenAsCharArray[i] != ' '){
                    givenAsCharArray[j] = givenAsCharArray[i];
                    i--;
                }else{
                    givenAsCharArray[j] = '0';
                    givenAsCharArray[j-1] = '2';
                    givenAsCharArray[j-2] = '%';
                    j-=2;
                    i--;
                }
            }
            return new string(givenAsCharArray);
        }

    }
}