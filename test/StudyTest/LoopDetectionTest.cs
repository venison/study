using System;
using System.Collections.Generic;
using Xunit;

namespace StudyTest
{
    public class LoopDetectionTest
    {
        private LoopDetection testObj;

        public LoopDetectionTest()
        {
            testObj = new LoopDetection();
        }

        [Fact]
        public void DegenerateTest()
        {                       
            var loopNode = testObj.FindLoop(null);            
            Assert.Equal(null, loopNode);
        }

        [Fact]
        public void FindLoop_GivenListWithNoLoop_ReturnsNull(){
            MyLinkedList<int> list = new MyLinkedList<int>(new List<int>{1,2,3,4,5});
            
            var loopNode = testObj.FindLoop(list);            
            
            Assert.Equal(null, loopNode);
        }

        [Fact]
        public void FindLoop_GivenLoopExists_ReturnsGuiltyNode(){
            MyLinkedList<int> list = new MyLinkedList<int>(new List<int>{1,2,3,4,5});
            var guiltyNode = new MyLinkedListNode<int>{Value=100};            
            list.InsertAtEnd(guiltyNode);
            guiltyNode.Next = list.Head;            
            
            var loopNode = testObj.FindLoop(list);            
            
            Assert.Equal(list.Head.Value, loopNode.Value);
        }

    }

    internal class LoopDetection
    {
        public LoopDetection()
        {
        }

        internal MyLinkedListNode<int> FindLoop(MyLinkedList<int> list)
        {
            if(list == null){
                return null;
            }
            var nodeHash = new HashSet<MyLinkedListNode<int>>();
            var runner = list.Head;
            while(runner.Next != null){
                if(nodeHash.Contains(runner)){
                    return runner;
                }else{
                    nodeHash.Add(runner);
                }
                runner = runner.Next;
            }
            
            return null;
        }
    }
}