using System;
using System.Collections.Generic;
using Xunit;

namespace StudyTest
{
    public class ZeroMatrixTest
    {
        private ZeroMatrix testObj;

        public ZeroMatrixTest()
        {
            testObj = new ZeroMatrix();
        }

        [Fact]
        public void DegenerateTest()
        {
            testObj.Zero(null);
            testObj.Zero(new int[0,0]);
        }

        [Fact]
        public void ZeroOut_GivenMatrixWithZeroes_ExpectsZeroes()
        {
            var givenMatrix = new int[,]{{0,0}};
            var expectedMatrix = new int[,]{{0,0}};
            
            testObj.Zero(givenMatrix);

            Assert.Equal(expectedMatrix, givenMatrix);
        }

        [Fact]
        public void ZeroOut_GivenMatrixWithNoZeroes_ExpectsNoZeroes(){
            var givenMatrix = new int[,]{{1,1}};
            var expectedMatrix = new int[,]{{1,1}};
            
            testObj.Zero(givenMatrix);

            Assert.Equal(expectedMatrix, givenMatrix);
        }

        [Fact]
        public void Zero_GivenOneRowWithZero_ExpectsFullRowZeroed(){
            var givenMatrix = new int[,]   {{1,0,1,1,1,1}};
            var expectedMatrix = new int[,]{{0,0,0,0,0,0}};

            testObj.Zero(givenMatrix);

            Assert.Equal(expectedMatrix, givenMatrix);
        }

        [Fact]
        public void Zero_GivenMultipleRowsRowWithZero_ExpectsFullColumnZeroed(){
            var givenMatrix = new int[,]   {
                 {1,0,1}
                ,{1,1,1}
                ,{1,1,1}
                };
            var expectedMatrix = new int[,]{
                 {0,0,0}
                ,{1,0,1}
                ,{1,0,1}
                };

            testObj.Zero(givenMatrix);

            Assert.Equal(expectedMatrix, givenMatrix);
        }


    }

    internal class ZeroMatrix
    {
        public ZeroMatrix()
        {
        }

        internal void Zero(int[,] matrix)
        {
            if(null == matrix || matrix.Length <= 0){
                return;
            }
            var zeroes = FindZeroes(matrix);
            for(int i=0; i<=zeroes.X.Length-1;i++){
                ZeroOutVertically(zeroes.X[i], i, matrix);
            }

            for(int j=0; j<=zeroes.Y.Length-1;j++){
                ZeroOutHorizontally(zeroes.Y[j], j, matrix);
            }
        }

        private void ZeroOutHorizontally(int zeroFound, int row, int[,] matrix)
        {
            if(zeroFound != 1){
                return;
            }
            var rowLength = matrix.GetUpperBound(1);
            for(int i=0; i<= rowLength; i++){
                matrix[row,i] = 0;
            }
        }

        private void ZeroOutVertically(int zeroFound, int column, int[,] matrix)
        {
            if(zeroFound != 1){
                return;
            }
            var columnHeight = matrix.GetUpperBound(0);
            for(int i=0; i<= columnHeight; i++){
                matrix[i,column] = 0;
            }
        }

        private XYs FindZeroes(int[,] matrix)
        {
            var columnHeight = matrix.GetUpperBound(0);
            var rowLength = matrix.GetUpperBound(1);
            var zeroes = new XYs();
            zeroes.X = new int[rowLength+1];
            zeroes.Y = new int[columnHeight+1];
            for(int i=0; i<= columnHeight; i++){
                for(int j=0; j<= rowLength; j++){
                    if(0 == matrix[i,j]){
                        zeroes.X[j] = 1;
                        zeroes.Y[i] = 1;
                    }
                }
            }
            return zeroes;
        }
    }

    internal class XYs
    {
        public int[] X;
        public int[] Y;
    }
}