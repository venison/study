using System;
using System.Collections.Generic;
using Xunit;

namespace StudyTest
{
    public class StringRotationTest
    {
        private StringRotation testObj;

        public StringRotationTest()
        {
            testObj = new StringRotation();
        }

        [Fact]
        public void DegenerateTest()
        {
            Assert.False(testObj.IsRotation(null, null));
            Assert.False(testObj.IsRotation(string.Empty, string.Empty));
        }

        [Fact]
        public void IsRotation_GivenNonRotationWords_ReturnsFalse(){
            Assert.False(testObj.IsRotation("fish", "tacos"));
        }

        [Fact]
        public void IsRotation_GivenRotationWord_ReturnsTrue(){
            Assert.True(testObj.IsRotation("fish", "shfi"));
        }

    }

    internal class StringRotation
    {
        public StringRotation()
        {
        }
       
        internal bool IsRotation(string baseLine, string possibleRotation)
        {
            if(string.IsNullOrEmpty(baseLine) || string.IsNullOrEmpty(possibleRotation)){
                return false;
            }
            
            var doubleRainbow = possibleRotation + possibleRotation;
            if(doubleRainbow.Contains(baseLine))
            {
                return true;
            }
            return false;
        }
    }
}