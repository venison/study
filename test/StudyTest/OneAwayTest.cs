using System;
using System.Collections.Generic;
using Xunit;

namespace StudyTest
{
    public class OneAwayTest
    {
        private OneAway testObj;

        public OneAwayTest()
        {
            testObj = new OneAway();
        }

        [Fact]
        public void DegenerateTest()
        {
            Assert.True(testObj.OneEdit(null, null));
            Assert.True(testObj.OneEdit("", ""));
        }

        [Fact]
        public void Given_StringsTheSame_ExpectsTrue(){
            Assert.True(testObj.OneEdit("a", "a"));
        }

        [Fact]
        public void Given_StringsTwoCharactersLonger_ExpectsFalse()
        {
            Assert.False(testObj.OneEdit("", "ab"));
        }

        [Fact]
        public void Given_StringOneCharacterLonger_ExectsTrue(){
            Assert.True(testObj.OneEdit("", "a"));
        }

        [Theory]
        [InlineData("pale", "ple", true)]
        [InlineData("pale", "pales", true)]
        [InlineData("pale", "bale", true)]
        [InlineData("pale", "bake", false)]
        
        public void GivenCases_ExpectsProperResults(string first, string second, bool expectation ){
            Assert.Equal(expectation, testObj.OneEdit(first, second));
        }
    }

    internal class OneAway
    {
        public OneAway()
        {
        }

        internal bool OneEdit(string first, string second)
        {
            if(first == second){
                return true;
            }
            if(Math.Abs(first.Length - second.Length) >= 2)
            {
                return false;
            }
           if(first.Length == second.Length)
           {
               return HandleStringsEqualLengthCase(first, second);

           }else{
                return HandleStringsDifferInLengthCase(first, second);
           }
        }

        private bool HandleStringsDifferInLengthCase(string first, string second)
        {
            var shortStraw = first;
            var longStraw = second;
            if(first.Length>second.Length)
            {
                shortStraw = second;
                longStraw = first;
            }
            var badCount = 0;
            var j = 0;
            for(var i=0; i<longStraw.Length-1; i++){
                if(shortStraw[j] != longStraw[i]){
                    badCount++;                    
                }else{
                    j++;
                }

                if(badCount>=2)
                {
                    return false;
                }
            }

            return true;
        }

        private bool HandleStringsEqualLengthCase(string first, string second)
        {
            var badCount = 0;
             
            for(var i=0; i<first.Length-1; i++){
                if(first[i] != second[i]){
                    badCount++;                    
                }

                if(badCount>=2)
                {
                    return false;
                }
            }
            return true;
        }
    }
}