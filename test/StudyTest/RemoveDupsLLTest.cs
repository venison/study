using System;
using System.Collections.Generic;
using Xunit;

namespace StudyTest
{
    public class RemoveDupsLLTest
    {
        private LLDupRemover testObj;

        public RemoveDupsLLTest()
        {
            testObj = new LLDupRemover();
        }

        [Fact]
        public void DegenerateTest()
        {
            LinkedList<int> expected = null;
            LinkedList<int> actual = null;
            
            testObj.DedupifyUsingHash(actual);

            Assert.Equal(expected, actual);            
        }

        [Fact]
        public void Dedupify_GivenNoDups_ExpectsNothingRemoved()
        {
            LinkedList<int> expected = new LinkedList<int>();
            expected.AddFirst(100);
            expected.AddFirst(6);
            expected.AddFirst(7);
            LinkedList<int> actual = new LinkedList<int>();
            actual.AddFirst(100);
            actual.AddFirst(6);
            actual.AddFirst(7);            
            
            testObj.DedupifyUsingHash(actual);

            Assert.Equal(expected, actual);            
        }

        [Fact]
        public void Dedupify_GivenDups_ExpectsRemoved()
        {
            LinkedList<int> expected = new LinkedList<int>();
            expected.AddFirst(100);
            expected.AddFirst(6);
            
            LinkedList<int> actual = new LinkedList<int>();
            actual.AddFirst(100);
            actual.AddFirst(6);            
            actual.AddFirst(6);

            testObj.DedupifyUsingHash(actual);

            Assert.Equal(expected, actual);            
        }

        [Fact]
        public void DedupifyNoHash_GivenDups_ExpectsRemoved()
        {
            LinkedList<int> expected = new LinkedList<int>();
            expected.AddFirst(100);
            expected.AddFirst(6);
            
            LinkedList<int> actual = new LinkedList<int>();
            actual.AddFirst(100);
            actual.AddFirst(6);            
            actual.AddFirst(6);

            testObj.DedupifyNoExtraDatastructure(actual);

            Assert.Equal(expected, actual);            
        }


    }

    internal class LLDupRemover
    {
        public LLDupRemover()
        {
        }

        internal void DedupifyNoExtraDatastructure(LinkedList<int> actual)
        {
            var slow = actual.First;            
            while(null != slow ){

                var fast = slow.Next;
                while(null != fast)
                {
                    if(slow.Value == fast.Value)
                    {
                        actual.Remove(fast);
                    }
                    fast = fast.Next;
                }
                slow = slow.Next;                                
            }
        }

        internal void DedupifyUsingHash(LinkedList<int> given)
        {
            if(null == given ){
                return;
            }

            var numbersSoFar = new HashSet<int>();
            var current = given.First;
            while(current != null){
                if(numbersSoFar.Contains(current.Value)){
                    //remove this node
                    given.Remove(current);
                }else{
                    numbersSoFar.Add(current.Value);
                }
                current = current.Next;
            }            
        }


    }
}