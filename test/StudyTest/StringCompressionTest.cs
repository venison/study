using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace StudyTest
{
    public class StringCompressionTest
    {
        private StringCompressor testObj;

        public StringCompressionTest()
        {
            testObj = new StringCompressor();
        }

        [Fact]
        public void Compress_GivenBaseCases_ReturnsDegenerateGivens()
        {
            Assert.Equal(null, testObj.Compress(null));
            Assert.Equal(string.Empty, testObj.Compress(string.Empty));
        }

        [Fact]
        public void Compress_GivenStringThatIsShorterThanCompressedVersion_ExpectsGivenString()
        {
            var givenString = "abcdefg";
            Assert.Equal(givenString, testObj.Compress(givenString));
        }

        [Theory]
        [InlineData("aaa", "a3")]
        [InlineData("aabbbccccd","a2b3c4d1")]
        [InlineData("aabcccccaaa","a2b1c5a3")]
        public void Compress_GivenString_ExpectsCorrectCompression(string given, string expected)
        {
            Assert.Equal(expected, testObj.Compress(given));
        }
    }

    internal class StringCompressor
    {
        public StringCompressor()
        {
        }

        internal string Compress(string given)
        {
            if(string.IsNullOrEmpty(given)){
                return given;
            }

            var charArray = given.ToCharArray();
            var stringBuilder = new StringBuilder();
            for(int i=0; i<charArray.Length; )
            {
                var currentCharacter = charArray[i];
                stringBuilder.Append(currentCharacter);
                
                var currentIndex = i+1;                
                var currentCount = 1;
                while(currentIndex <= charArray.Length-1 && charArray[currentIndex] == currentCharacter){
                    currentCount++;         
                    currentIndex++;           
                }                
                i=currentIndex;
                
                stringBuilder.Append(currentCount);
            }

            if(given.Length < stringBuilder.Length)
            {
                return given;
            }

            return stringBuilder.ToString();
        }
    }
}
