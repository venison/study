using System;
using System.Collections.Generic;
using Xunit;

namespace StudyTest
{
    public class PalindromePermutationTest
    {
        private PalindromePermutation testObj;

        public PalindromePermutationTest()
        {
            testObj = new PalindromePermutation();
        }

        [Fact]
        public void IsPerm_GivenDegenerateCases_ExpectsTrue()
        {
            string first = null;
            

            Assert.True(testObj.IsPal(first));

            first = "";
            

            Assert.True(testObj.IsPal(first));
        }

        [Fact]
        public void IsPerm_GivenStringWithSpaces_ExpectsTrue()
        {
            string first = " a";            

            Assert.True(testObj.IsPal(first));
        }

        [Fact]
        public void IsPerm_GivenInternalSpaces_ExpectsTrue(){
            string first = "a a";            

            Assert.True(testObj.IsPal(first));
        }

        [Fact]
        public void IsPerm_GivenNotPerm_ExpectsFalse()
        {
            string first = "ab";

            Assert.False(testObj.IsPal(first));
        }

        [Theory]
        [InlineData("aba", true)]
        [InlineData("tact coa", true)]
        [InlineData("aaa", true)]
        [InlineData("abb", true)]
        [InlineData("aabb", true)]
        public void IsPerm_GivenCases_ExpectsProperAnswer(string given, bool expected){
            Assert.Equal(expected, testObj.IsPal(given));
        }

    }

    internal class PalindromePermutation
    {
        public PalindromePermutation()
        {
        }

        internal bool IsPal(string first)
        {
            if(string.IsNullOrEmpty(first)){
                return true;
            }
                       
            var characters = new Dictionary<char, int>();
            var lengthWithoutSpaces = 0;
            foreach(var character in first)
            {
                if(character != ' ')
                {
                    if(characters.ContainsKey(character))
                    {
                        characters[character]++;
                    }else{
                        characters.Add(character, 1);
                    }
                    lengthWithoutSpaces++;
                }
            }

            if(lengthWithoutSpaces % 2 == 0)
            {
                foreach(var kvp in characters)
                {
                    if(kvp.Value % 2 != 0)
                    { 
                        return false;
                    }
                }                
            }else{
                bool foundAnOddity = false;
                foreach(var kvp in characters)
                {
                    if(kvp.Value % 2 != 0)
                    { 
                        if(foundAnOddity){
                            return false;
                        }else{
                            foundAnOddity = true;
                        }
                    }
                }                
            }
            return true;
            
        }
    }
}