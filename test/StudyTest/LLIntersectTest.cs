using System;
using System.Collections.Generic;
using Xunit;

namespace StudyTest
{
    public class LLIntersectTest
    {
        private LLIntersect testObj;

        public LLIntersectTest()
        {
            testObj = new LLIntersect();
        }

        [Fact]
        public void DegenerateTest()
        {                       
            var actual = testObj.Intersect(null, null);            
            Assert.Equal(null, actual);
        }

        [Fact]
        public void Intersect_GivenNonIntersectingLists_ExpectsNull()
        {
            var first = new MyLinkedList<int>(new List<int> { 1, 2, 3 });
            var second = new MyLinkedList<int>(new List<int> { 1, 2, 3 });
            
            var actual = testObj.Intersect(first, second);

            Assert.Equal(null, actual);
        }

        [Fact]
        public void Intersect_GivenIntersectingLists_ExpectsNode()
        {
            var first = new MyLinkedList<int>();
            var second = new MyLinkedList<int>();

            var intersectingNode = new MyLinkedListNode<int>{Value=6};
            intersectingNode.Next = new MyLinkedListNode<int>{Value=7}; 

            first.AddFirst(5);
            second.AddFirst(99);
            first.InsertAtEnd(intersectingNode);
            second.InsertAtEnd(intersectingNode);
            
            var actual = testObj.Intersect(first, second);

            Assert.Equal(intersectingNode, actual);
        }

        [Fact]
        public void Intersect_GivenIntersectingListsWithDifferentLengths_ExpectsNode()
        {
            var first = new MyLinkedList<int>(new List<int>{5,1,2,3,4});
            var second = new MyLinkedList<int>(new List<int>{99});
            var intersectingNode = new MyLinkedListNode<int>{Value=6};
            intersectingNode.Next = new MyLinkedListNode<int>{Value=7}; 
            intersectingNode.Next.Next = new MyLinkedListNode<int>{Value=100}; 
            first.InsertAtEnd(intersectingNode);
            second.InsertAtEnd(intersectingNode);
            
            var actual = testObj.Intersect(first, second);

            Assert.Equal(intersectingNode, actual);
        }

    }

    internal class LLIntersect
    {
        public LLIntersect()
        {
        }

        internal MyLinkedListNode<int> Intersect(MyLinkedList<int> firstList, MyLinkedList<int> secondList)
        {
            if(firstList == null || secondList == null){
                return null;
            }

           if(!ListsIntersect(firstList, secondList)){
                return null;
            }
            
            var longerList = firstList;
            var shorterList = secondList;
            if(longerList.Count<shorterList.Count){
                longerList = secondList;
                shorterList = firstList;
            }
            var runnerHeadStart = longerList.Count - shorterList.Count;
            var longerListRunner = AdvanceRunner(longerList.Head, runnerHeadStart);
            return FindIntersectNode(longerListRunner, shorterList.Head);
        }

        private bool ListsIntersect(MyLinkedList<int> firstList, MyLinkedList<int> secondList)
        {
            var firstEnd = FindEnd(firstList);
            var secondEnd = FindEnd(secondList);
            return firstEnd == secondEnd;
        }

        private MyLinkedListNode<int> FindIntersectNode(MyLinkedListNode<int> firstRunner, MyLinkedListNode<int> secondRunner)
        {
            var currentFirst = firstRunner;
            var currentSecond = secondRunner;
            while(currentFirst != currentSecond){
                currentFirst = currentFirst.Next;
                currentSecond = currentSecond.Next;
            }
            return currentFirst;
        }

        private MyLinkedListNode<int> AdvanceRunner(MyLinkedListNode<int> head, int runnerHeadStart)
        {
            var runner = head;
            for(int i=0; i< runnerHeadStart; i++){
                runner = runner.Next;
            }
            return runner;
        }

        private MyLinkedListNode<int> FindEnd(MyLinkedList<int> list)
        {
            var current = list.Head;
            while(current.Next != null){
                current = current.Next;
            }
            return current;
        }
    }
}