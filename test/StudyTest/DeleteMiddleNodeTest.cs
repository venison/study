using System;
using Xunit;

namespace StudyTest
{
    public class DeleteMiddleNodeTest
    {
        private DeleteMiddleNode testObj;

        public DeleteMiddleNodeTest()
        {
            testObj = new DeleteMiddleNode();
        }

        [Fact]
        public void DegenerateTest()
        {                       
            testObj.Delete(null);            
        }

        [Fact]
        public void Delete_GivenNode_RemovesIt(){
            var givenNode = new MyLinkedListNode<int>{Value=98};
            var list = new MyLinkedList<int>();
            var first = new MyLinkedListNode<int>{Value=97};
            var second = new MyLinkedListNode<int>{Value=9};
            var last = new MyLinkedListNode<int>{Value=8};
            first.Next = second;
            second.Next = givenNode;
            givenNode.Next = last;
            last.Next = null;
            list.Head = first;

            testObj.Delete(givenNode);

            Assert.Equal(first.Value, list.Head.Value);
            Assert.Equal(second.Value, first.Next.Value);
            Assert.Equal(last.Value, second.Next.Value);
            Assert.Equal(null, last.Next);
        }

    }

    internal class DeleteMiddleNode
    {
        public DeleteMiddleNode()
        {
        }

        internal void Delete(MyLinkedListNode<int> givenNode)
        {
            if(null == givenNode){
                return;
            }
            var next = givenNode.Next;
            givenNode.Value = next.Value;
            givenNode.Next = next.Next;            
        }
    }
}