using System;
using System.Collections.Generic;
using Xunit;

namespace StudyTest
{
    public class StringPermutationTest
    {
        private StringPermutation testObj;

        public StringPermutationTest()
        {
            testObj = new StringPermutation();
        }

        [Fact]
        public void IsPerm_GivenDegenerateCases_ExpectsTrue()
        {
            Assert.True(testObj.IsPerm(null, null));
            Assert.True(testObj.IsPerm(string.Empty, string.Empty));
            
        }

        [Fact]
        public void IsPerm_GivenTwoSameString_ExpectsTrue(){
            Assert.True(testObj.IsPerm("a", "a"));
        }

        [Fact]
        public void IsPerm_GivenTwoDifferentStrings_ExpectsFalse(){
            Assert.False(testObj.IsPerm("a", "b"));

        }

        [Fact]
        public void IsPerm_GivenTwoDifferentLengthStrins_ExpectsFalse(){
            Assert.False(testObj.IsPerm("a", "ab"));
        }

        [Fact]
        public void IsPerm_GivenPermutationn_ExpectsTrue(){
            Assert.True(testObj.IsPerm("ab", "ba"));
            Assert.True(testObj.IsPerm("bab", "bba"));
            Assert.True(testObj.IsPerm("aczzb", "zbacz"));
        }

    }

    internal class StringPermutation
    {
        //Another solution that might be more straight forward would be to sort the strings first and then compare them.  (this may not be super optimal, but it is super understandable)   
        internal bool IsPerm(string first, string second)
        {
            if(first == second){
                return true;
            }

            if(first.Length != second.Length){
                return false;
            }

            var count = new Dictionary<char, int>();
            foreach(var character in first)
            {
                if(count.ContainsKey(character)){
                    count[character] ++;
                }else{
                    count.Add(character, 1);
                }
            }

            foreach(var search in second){
                if(!count.ContainsKey(search)){
                    return false;
                }
                count[search]--;
                if(count[search] == 0){
                    count.Remove(search);
                }
            }
            if(count.Count <= 0){
                return true;
            }
            return false;
        }
    }
}