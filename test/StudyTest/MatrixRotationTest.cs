using System;
using System.Collections.Generic;
using Xunit;

namespace StudyTest
{
    public class MatrixRotationTest
    {
        private MatrixRotation testObj;

        public MatrixRotationTest()
        {
            testObj = new MatrixRotation();
        }

        [Fact]
        public void DegenerateTest()
        {
            testObj.Rotate(null);
            testObj.Rotate(new int[0,0]);
            
        }

        [Fact]
        public void Rotate_GivenMatrix_rotatesIt(){
            var given = new int[,]{
                 {1,2,3}
                ,{4,5,6}
                ,{7,8,9}
            };

            var expected = new int[,]{
                 {7,4,1}
                ,{8,5,2}
                ,{9,6,3}
            };

            testObj.Rotate(given);
            
            Assert.Equal(expected, given );
        }

        [Fact]
        public void Rotate_GivenMatrixs_rotatesIt(){
            var given = new int[,]{
                 {1,2,3, 10}
                ,{4,5,6, 11}
                ,{7,8,9, 12}
                ,{21,22,23,24}
            };

            var expected = new int[,]{
                 {21, 7,4,    1}
                ,{22, 8,5,    2}
                ,{23, 9,6,    3}
                ,{24, 12, 11, 10}
            };

            testObj.Rotate(given);
            
            Assert.Equal(expected, given );
        }
    }

    internal class MatrixRotation
    {
        public MatrixRotation()
        {
        }

        internal void Rotate(int[,] matrix)
        {
            if(matrix == null){
                return;
            }
            var yBounds = matrix.GetUpperBound(0);
            var xBounds = matrix.GetUpperBound(1);
            var totalLength = matrix.Length;
            if(yBounds <= 0 || yBounds != xBounds){
                return;
            }

            for(int layer = 0; layer < yBounds+1/2; layer++){
                int firstLayer = layer;
                int lastLayer = yBounds  - layer;
                for(int i=firstLayer; i<lastLayer; i++){
                    int offset = i - firstLayer;
                    int top = matrix[firstLayer, i];
                    //left -> Top
                    matrix[firstLayer, i]  = matrix[lastLayer-offset, firstLayer];

                    //bottom -> left
                    matrix[lastLayer - offset, firstLayer] = matrix[lastLayer, lastLayer - offset];

                    //right -> bottom
                    matrix[lastLayer, lastLayer-offset] = matrix[i, lastLayer];

                    ///top -> right
                    matrix[i, lastLayer] = top;
                }

            }
        }
    }
}
