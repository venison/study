using Xunit;

namespace StudyTest
{
    public class IsUniqueTest
    {
        private IsUnique testObj;

        public IsUniqueTest()
        {
            testObj = new IsUnique();
        }

        [Fact]
        public void IsUnique_GivenNull_ExpectsTrue()
        {
            Assert.True(testObj.Is(null));
        }

        [Fact]
        public void IsUnique_GivenEmpty_ExpectsTrue(){
            Assert.True(testObj.Is(string.Empty));
        }

        [Fact]
        public void IsUnique_GivenOneCharaccter_ExpectsTrue()
        {
            Assert.True(testObj.Is("a"));    
        }

        [Fact]
        public void Is_GivenTwoOfSameCharacter_ExpectsFalse()
        {
            Assert.False(testObj.Is("aa"));
        }

        
    }

    public class IsUnique
    {
        private int bits;
        //private HashSet<char> characters = new HashSet<char>();
        public IsUnique()
        {
        }

        public bool Is(string given){
            if(string.IsNullOrEmpty(given)){
                return true;
            }

            if(given.Length <= 1){
                return true;
            }

            foreach(var character in given)
            {
                var intVal = character - 'a';
                var shifted = (1 << intVal);
                if((bits & shifted) > 0){
                    return false;
                }
                bits |= shifted;
            }

            return true;
        
        }
    //     public bool Is(string given)
    //     {
    //         if(string.IsNullOrEmpty(given)){
    //             return true;
    //         }

    //         if(given.Length <= 1){
    //             return true;
    //         }
    //         foreach(var character in given)
    //         {
    //             if(characters.Contains(character)){
    //                 return false;
    //             }else{
    //                 characters.Add(character);
    //             }
    //         }
    //         return true;
    //     }
    // }
    }
}
