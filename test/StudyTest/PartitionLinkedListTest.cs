using System;
using Xunit;

namespace StudyTest
{
    public class PartitionLinkedListTest
    {
        private PartitionLinkedList testObj;

        public PartitionLinkedListTest()
        {
            testObj = new PartitionLinkedList();
        }

        [Fact]
        public void DegenerateTest()
        {                       
            testObj.Partition(null, 5);            
        }

        [Fact]
        public void Partition_GivenValue_PartitionsLowerValuesFirst(){
            var value = 5;
            var lessNode = new MyLinkedListNode<int>{Value=4};
            var otherlessNode = new MyLinkedListNode<int>{Value=4};
            var greaterNode = new MyLinkedListNode<int>{Value=6};            
            var otherGreaterNode = new MyLinkedListNode<int>{Value=7};
            var list = new MyLinkedList<int>();
            list.Head = greaterNode;
            greaterNode.Next = otherGreaterNode;
            otherGreaterNode.Next = otherlessNode;
            otherlessNode.Next = lessNode;

            testObj.Partition(list, value);

            Assert.True(list.Head.Value < value);
            Assert.True(list.Head.Next.Value < value);
            Assert.True(list.Head.Next.Next.Value > value);
            Assert.True(list.Head.Next.Next.Next.Value > value);
        }        

    }

    internal class PartitionLinkedList
    {
        internal void Partition(MyLinkedList<int> givenList, int partition)
        {
            if(null == givenList)
            {
                return;
            }
            var current = givenList.Head;
            MyLinkedListNode<int> leftHead = null;
            MyLinkedListNode<int> currentLeft = null;
            MyLinkedListNode<int> rightHead = null;
            MyLinkedListNode<int> currentRight = null;


            while(null != current)
            {
                if(current.Value < partition){
                    if(leftHead == null){
                        leftHead = current;
                        currentLeft = current;
                    }else{
                        currentLeft.Next = current;
                        currentLeft = currentLeft.Next;                        
                    }
                    
                }else{
                    if(rightHead == null){
                        rightHead = current;
                        currentRight = current;
                    }else{                        
                        currentRight.Next = current;
                        currentRight = currentRight.Next;                                              
                    }
                }
                current = current.Next;
            }
            givenList.Head = leftHead;
            currentLeft.Next = rightHead;
            currentRight.Next = null;            
        }
    }
}