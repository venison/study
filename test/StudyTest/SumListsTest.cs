using System;
using System.Collections.Generic;
using Xunit;

namespace StudyTest
{
    public class PalindromeTest
    {
        private SumListsNoRecurse testObj;

        public PalindromeTest()
        {
            testObj = new SumListsNoRecurse();
        }

        [Fact]
        public void DegenerateTest()
        {                       
            var total = testObj.Sum(null, null);            
            Assert.Equal(null, total);
        }

        [Theory]
        [InlineData(5,4)]
        [InlineData(3,3)]
        public void Sum_GivenShortLists_SumsThem(int firstVal, int secondVal){
            var first = new MyLinkedList<int>();            
            var second = new MyLinkedList<int>();            
            var expected = new MyLinkedList<int>();
            first.AddFirst(firstVal);
            second.AddFirst(secondVal);
            expected.AddFirst(firstVal+secondVal);

            var actual = testObj.Sum(first, second);

            Assert.Equal(expected.First.Value, actual.First.Value);
        }

        [Fact]
        public void Sum_GivenTwoDigitNumbers_SumsThem()
        {
            var first = new MyLinkedList<int>(new List<int>{5,1});
            var second = new MyLinkedList<int>(new List<int>{1,1});
            var expected = new MyLinkedList<int>(new List<int>{6,2});

            var actual = testObj.Sum(first, second);

            Assert.Equal(expected.First.Value, actual.First.Value);
            Assert.Equal(expected.First.Next.Value, actual.First.Next.Value);
        }

        [Fact]
        public void Sum_GivenThreeDigitNumbers_SumsThem()
        {
            var first = new MyLinkedList<int>(new List<int>{7,1,6});
            var second = new MyLinkedList<int>(new List<int>{5,9,2});
            var expected = new MyLinkedList<int>(new List<int>{2,1,9});

            var actual = testObj.Sum(first, second);

            Assert.Equal(expected.First.Value, actual.First.Value);
            Assert.Equal(expected.First.Next.Value, actual.First.Next.Value);
            Assert.Equal(expected.First.Next.Next.Value, actual.First.Next.Next.Value);
        }
    }

    internal class SumListsRecurse
    {
        public MyLinkedList<int> Sum(MyLinkedList<int> firstList, MyLinkedList<int> secondList)
        {
            var returnVal = new MyLinkedList<int>();
            MyLinkedListNode<int> result = AddLists(firstList.First, secondList.First, 0);
            returnVal.AddFirst(result);
            return returnVal;
        }

        private MyLinkedListNode<int> AddLists(MyLinkedListNode<int> first, MyLinkedListNode<int> second, int carry)
        {
            if(first == null && second == null && carry == 0){
                return null;
            }

            var result = new MyLinkedListNode<int>();
            int value = carry;
            if(first != null){
                value += first.Value;
            }

            if(second != null){
                value += second.Value;
            }

            result.Value = value % 10;
            if(first != null || second != null){
                var firstNext = first == null ? null : first.Next;
                var secondNext = second == null ? null : second.Next;
                var nextValue = value >= 0 ? 1 : 0;//carry the 1, or not
                var more = AddLists(firstNext, secondNext, nextValue);
                result.Next = more;
            }


            return result;
            
        }
    }
    internal class SumListsNoRecurse
    {
        internal MyLinkedList<int> Sum(MyLinkedList<int> firstList, MyLinkedList<int> secondList)
        {
            if(firstList == null || secondList == null){
                return null;
            }

            var returnVal = new MyLinkedList<int>();
            var firstNumber = GetNumber(firstList);
            var secondNumber = GetNumber(secondList);
            var total = firstNumber+secondNumber;

            CreateListFrom(total, returnVal);

            return returnVal;
        }

        private int GetNumber(MyLinkedList<int> list)
        {
            var number = list.First.Value;
            var tens = list.Count-1;
            var currentNode = list.Last;
            while(list.First != currentNode){
                number += currentNode.Value*(int)Math.Pow(10,tens);
                tens--;
                currentNode = currentNode.Previous;
            }            
            return number;
        }

        private static void CreateListFrom(int givenValue, MyLinkedList<int> givenList)
        {
            if (givenValue < 10)
            {
                givenList.AddFirst(givenValue);
                return;
            }

            int tens = CountPowersOfTen(givenValue);
            for (int i = tens; i >= 1; i--)
            {
                int position = (int)Math.Pow(10 , i);//600
                int tmp = (int)Math.Floor((double)givenValue / position);//650/100 = 
                givenList.AddFirst(tmp);
                givenValue = givenValue - position * tmp;
                if (givenValue < 10)
                {
                    givenList.AddFirst(givenValue);
                }
            }
        }

        private static int CountPowersOfTen(int givenValue)
        {
            var tens = 0;
            var copy = givenValue;
            while (copy > 10)
            {
                tens++;
                copy = copy / 10;
            }

            return tens;
        }
    }
}