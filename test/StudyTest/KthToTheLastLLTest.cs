using System;
using System.Collections.Generic;
using Xunit;

namespace StudyTest
{
    public class KthToTheLastLLTest
    {
        private KthToTheLastLL testObj;

        public KthToTheLastLLTest()
        {
            testObj = new KthToTheLastLL();
        }

        [Fact]
        public void DegenerateTest()
        {
            LinkedList<int> givenList = null;            
            LinkedListNode<int> expectedNode = null;
            
            var actualNode = testObj.KthNode(0, givenList);

            Assert.Equal(expectedNode, actualNode);            
        }

        [Fact]
        public void Kth_GivenListLessThanK_ExpectsHead(){
            LinkedList<int> givenList = new LinkedList<int>();                        
            LinkedListNode<int> expectedNode = new LinkedListNode<int>(5);
            givenList.AddFirst(expectedNode);
            
            var actualNode = testObj.KthNode(7, givenList);

            Assert.Equal(expectedNode, actualNode);            
        }

        [Fact]
        public void Kth_GivenGoodListAndK_ExpectsKthNode(){
            LinkedList<int> givenList = new LinkedList<int>(); 
            LinkedListNode<int> expectedNode = new LinkedListNode<int>(5);
            givenList.AddFirst(expectedNode);
            givenList.AddFirst(68);
            givenList.AddFirst(86);
            givenList.AddAfter(expectedNode, 99);
            givenList.AddAfter(expectedNode, 100);
            //86 -> 68 -> 5 ->100 -> 99
            var k = 2;
            
            var actualNode = testObj.KthNode(k, givenList);

            Assert.Equal(expectedNode, actualNode);            
        }
    }

    internal class KthToTheLastLL
    {
        public KthToTheLastLL()
        {
        }

        internal LinkedListNode<int> KthNode(int k, LinkedList<int> givenList)
        {
            if(null == givenList){
                return null;
            }
            if(k>= givenList.Count){
                return givenList.First;
            }
            var runner = givenList.First;
            var kthNode = givenList.First;
            for(int i=0; i<=k; i++){
                runner = runner.Next;
            }
            while(null != runner){
                runner = runner.Next;
                kthNode = kthNode.Next;
            }
            return kthNode;
        }
    }
}