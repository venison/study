using System;
using System.Collections.Generic;
using Xunit;

namespace StudyTest
{
    public class LLPalindromeTest
    {
        private LLPalindrome testObj;

        public LLPalindromeTest()
        {
            testObj = new LLPalindrome();
        }

        [Fact]
        public void DegenerateTest()
        {                       
            var actual = testObj.IsPal(null);            
            Assert.False(actual);
        }

        [Fact]
        public void GivenSingleItem_ExpectsTrue(){
            var oneItem = new MyLinkedList<int>(new List<int>{1});

            var actual = testObj.IsPal(oneItem);
            
            Assert.True(actual);
        }

        [Fact]
        public void GivenTwoItemsDifferent_ExpectsFalse()
        {
            var twoItems = new MyLinkedList<int>(new List<int>{1,2});

            var actual = testObj.IsPal(twoItems);
            
            Assert.False(actual);
        }

        [Fact]
        public void GivenTwoSameItems_ExpectsTrue()
        {
            var twoItems = new MyLinkedList<int>(new List<int>{1,1});

            var actual = testObj.IsPal(twoItems);
            
            Assert.True(actual);
        }

        [Fact]
        public void GivenLongOddlengthPalindrome_ExpectsTrue()
        {
            var twoItems = new MyLinkedList<int>(new List<int>{1,2,3,4,5,4,3,2,1});

            var actual = testObj.IsPal(twoItems);
            
            Assert.True(actual);
        }

        [Fact]
        public void GivenLongEvenlengthPalindrome_ExpectsTrue()
        {
            var twoItems = new MyLinkedList<int>(new List<int>{1,2,3,4,5,5,4,3,2,1});

            var actual = testObj.IsPal(twoItems);
            
            Assert.True(actual);
        }

    }

    internal class LLPalindrome
    {
        public LLPalindrome()
        {
        }

        internal bool IsPal(MyLinkedList<int> list)
        {
            if (list == null)
            {
                return false;
            }

            if (list.Count == 1)
            {
                return true;
            }

            return CheckRecursive(list.Head, list.Last);
        }

        private bool CheckRecursive(MyLinkedListNode<int> front, MyLinkedListNode<int> back)
        {
            //odd length
            if(front == back){
                return true;
            }
            //even length
            if(front.Next == back){
                if(front.Value != back.Value){
                    return false;
                }
                return true;
            }
            if(front.Value != back.Value){
                return false;
            }
            return CheckRecursive(front.Next, back.Previous);
        }

        private bool CountApproach(MyLinkedList<int> list)
        {
            var counts = new Dictionary<int, int>();
            DoCounting(list.Head, counts);
            if (IsEven(list.Count))
            {
                foreach (var kvp in counts)
                {
                    if (!IsEven(kvp.Value))
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                var odds = 0;
                foreach (var kvp in counts)
                {
                    if (!IsEven(kvp.Value))
                    {
                        odds++;
                    }

                    if (odds >= 2)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        private static bool IsEven(int item)
        {
            return item % 2 == 0;
        }

        private void DoCounting(MyLinkedListNode<int> node, Dictionary<int,int> counts)
        {
            if(node == null){
                return;
            }
            if(counts.ContainsKey(node.Value)){
                counts[node.Value]++;
            }else{
                counts.Add(node.Value, 1);
            }
            DoCounting(node.Next, counts);
        }
    }
}