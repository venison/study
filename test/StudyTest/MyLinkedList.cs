using System.Collections.Generic;

namespace StudyTest
{
    public class MyLinkedList<T>{
        public MyLinkedListNode<T> First { get{ return Head;} set{Head = value;}}

        public int Count { get; internal set; }

        public MyLinkedListNode<T> Head;
        public MyLinkedListNode<T> Last;

        public MyLinkedList(){

        }

        public MyLinkedList(List<T> list)
        {            
            AddFirst(list[0]);            
            list.RemoveAt(0);            
            foreach(var item in list){
                var newNode = new MyLinkedListNode<T>{Value=item};
                InsertAtEnd(newNode);
            }
            
        }

        public void InsertAtEnd(MyLinkedListNode<T> newNode){
            InsertAtEnd(Head, newNode);
        }
        private void InsertAtEnd(MyLinkedListNode<T> start, MyLinkedListNode<T> newNode)
        {
            var current = start;
            Count++;
            while(current.Next != null){
                current = current.Next;                
            }
            current.Next = newNode;
            newNode.Previous = current;
            if(newNode.Next == null){
                Last = newNode;
            }else{
                CountAndSetLast(newNode);
            }
            
        }

        private void CountAndSetLast(MyLinkedListNode<T> newNode)
        {
            var current = newNode;
            while(current.Next != null){
                current = current.Next;
                Count++;
            }
            Last = current;
        }

        public void AddFirst(MyLinkedListNode<T> node){
            if(Head == null){
                Head = node;
                Last = First;
            }else{
                node.Next = Head;
                Head = node;
            }
            Count++;
        }

        public void AddFirst(T nodeValue){
            AddFirst(new MyLinkedListNode<T>{Value=nodeValue});
        }
    }

    public class MyLinkedListNode<T>{
        public T Value;
        public MyLinkedListNode<T> Next;

        public MyLinkedListNode<T> Previous;
    }
}